package com.tpSI.gNote.Entities;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "etudiant_cours")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Inscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "etudiant_id", nullable = false)
    private Etudiant etudiant;

    @ManyToOne
    @JoinColumn(name = "cours_id", nullable = false)
    private Cours cours;

}
