package com.tpSI.gNote.Entities;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "enseignant_cours")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EnseignantCours {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "enseignant_id", nullable = false)
    private Enseignant enseignant;

    @ManyToOne
    @JoinColumn(name = "cours_id", nullable = false)
    private Cours cours;
}
