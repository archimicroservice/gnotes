package com.tpSI.gNote.Controllers;



import com.tpSI.gNote.Entities.Cours;
import com.tpSI.gNote.Services.CoursService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cours")
public class CoursController {

    @Autowired
    private CoursService coursService;

    @PostMapping("/create")
    public Cours createCours(@RequestBody Cours cours) {
        return coursService.createCours(cours);
    }

    @GetMapping("/all")
    public List<Cours> getAllCours() {
        return coursService.getAllCours();
    }

    @GetMapping("/{coursId}")
    public Optional<Cours> getCoursById(@PathVariable Long coursId) {
        return coursService.getCoursById(coursId);
    }
}
