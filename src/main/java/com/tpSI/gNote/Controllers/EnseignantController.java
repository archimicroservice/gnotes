package com.tpSI.gNote.Controllers;


import com.tpSI.gNote.Entities.Enseignant;
import com.tpSI.gNote.Services.EnseignantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/enseignants")
public class EnseignantController {

    @Autowired
    private EnseignantService enseignantService;

    @PostMapping("/create")
    public Enseignant createEnseignant(@RequestBody Enseignant enseignant) {
        return enseignantService.createEnseignant(enseignant);
    }

    @GetMapping("/all")
    public List<Enseignant> getAllEnseignants() {
        return enseignantService.getAllEnseignants();
    }

    @GetMapping("/{enseignantId}")
    public Optional<Enseignant> getEnseignantById(@PathVariable Long enseignantId) {
        return enseignantService.getEnseignantById(enseignantId);
    }
}
