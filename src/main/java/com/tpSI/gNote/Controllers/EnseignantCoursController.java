package com.tpSI.gNote.Controllers;
import com.tpSI.gNote.Entities.EnseignantCours;
import com.tpSI.gNote.Services.EnseignantCoursService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/enseignant-cours")
public class EnseignantCoursController {
    @Autowired
    private EnseignantCoursService enseignantCoursService;

    @PostMapping("/save")
    public EnseignantCours saveEnseignantCours(@RequestBody EnseignantCours enseignantCours) {
        return enseignantCoursService.saveEnseignantCours(enseignantCours);
    }

    @GetMapping("/course/{courseId}")
    public List<EnseignantCours> getEnseignantCoursByCourse(@PathVariable Long courseId) {
        return enseignantCoursService.getEnseignantCoursByCourse(courseId);
    }

    @GetMapping("/enseignant/{enseignantId}")
    public List<EnseignantCours> getEnseignantCoursByEnseignant(@PathVariable Long enseignantId) {
        return enseignantCoursService.getEnseignantCoursByEnseignant(enseignantId);
    }

    @GetMapping("/{enseignantCoursId}")
    public Optional<EnseignantCours> getEnseignantCoursById(@PathVariable Long enseignantCoursId) {
        return enseignantCoursService.getEnseignantCoursById(enseignantCoursId);
    }
}
