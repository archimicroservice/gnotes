package com.tpSI.gNote.Controllers;

import com.tpSI.gNote.Entities.Note;
import com.tpSI.gNote.Services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/notes")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @PostMapping("/save")
    public Note saveNote(@RequestBody Note note) {
        return noteService.saveNote(note);
    }

    @GetMapping("/student/{studentId}/course/{courseId}")
    public List<Note> getNotesByStudentAndCourse(@PathVariable Long studentId, @PathVariable Long courseId) {
        return noteService.getNotesByStudentAndCourse(studentId, courseId);
    }

    @GetMapping("/average/{studentId}")
    public Double calculateAverageForStudent(@PathVariable Long studentId) {
        return noteService.calculateAverageNote(studentId);
    }
}
