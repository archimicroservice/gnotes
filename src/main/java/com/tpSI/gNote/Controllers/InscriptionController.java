package com.tpSI.gNote.Controllers;
import com.tpSI.gNote.Entities.Inscription;
import com.tpSI.gNote.Services.InscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inscriptions")
public class InscriptionController {

    @Autowired
    private InscriptionService inscriptionService;

    @PostMapping("/save")
    public Inscription saveInscription(@RequestBody Inscription inscription) {
        return inscriptionService.saveInscription(inscription);
    }

    @GetMapping("/course/{courseId}")
    public List<Inscription> getInscriptionsByCourse(@PathVariable Long courseId) {
        return inscriptionService.getInscriptionsByCourse(courseId);
    }

    @GetMapping("/student/{studentId}")
    public List<Inscription> getInscriptionsByStudent(@PathVariable Long studentId) {
        return inscriptionService.getInscriptionsByStudent(studentId);
    }
}
