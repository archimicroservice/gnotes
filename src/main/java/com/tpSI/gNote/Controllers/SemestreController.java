package com.tpSI.gNote.Controllers;


import com.tpSI.gNote.Entities.Semestre;
import com.tpSI.gNote.Services.SemestreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/semestres")
public class SemestreController {
    @Autowired
    private SemestreService semestreService;

    @GetMapping("/all")
    public List<Semestre> getAllSemestres() {
        return semestreService.getAllSemestres();
    }

    @GetMapping("/{semestreId}")
    public Optional<Semestre> getSemestreById(@PathVariable Long semestreId) {
        return semestreService.getSemestreById(semestreId);
    }
}
