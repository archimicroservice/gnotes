package com.tpSI.gNote.Repositories;


import com.tpSI.gNote.Entities.Note;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoteRepository extends JpaRepository<Note, Long> {
    public List<Note> findByEtudiantIdAndCoursId(Long studentId, Long courseId);

    List<Note> findByEtudiantId(Long studentId);
}
