package com.tpSI.gNote.Repositories;


import com.tpSI.gNote.Entities.EnseignantCours;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnseignantCoursRepository extends JpaRepository<EnseignantCours, Long> {

    public List<EnseignantCours> findByCoursId(Long courseId);
    public List<EnseignantCours> findByEnseignantId(Long enseignantId);
}
