package com.tpSI.gNote.Repositories;


import com.tpSI.gNote.Entities.Enseignant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnseignantRepository extends JpaRepository<Enseignant, Long> {
}
