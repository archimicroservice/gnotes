package com.tpSI.gNote.Repositories;


import com.tpSI.gNote.Entities.Cours;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoursRepository extends JpaRepository<Cours, Long> {
}
