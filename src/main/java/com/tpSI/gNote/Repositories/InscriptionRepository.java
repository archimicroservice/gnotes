package com.tpSI.gNote.Repositories;

import com.tpSI.gNote.Entities.Inscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InscriptionRepository extends JpaRepository<Inscription, Long> {
    public List<Inscription> findByCoursId(Long courseId);

    public List<Inscription> findByEtudiantId(Long studentId);
}
