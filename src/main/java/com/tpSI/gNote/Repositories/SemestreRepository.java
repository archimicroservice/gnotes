package com.tpSI.gNote.Repositories;

import com.tpSI.gNote.Entities.Semestre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SemestreRepository extends JpaRepository<Semestre, Long> {
}
