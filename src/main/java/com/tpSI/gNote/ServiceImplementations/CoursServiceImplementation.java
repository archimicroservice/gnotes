package com.tpSI.gNote.ServiceImplementations;

import com.tpSI.gNote.Entities.Cours;
import com.tpSI.gNote.Repositories.CoursRepository;
import com.tpSI.gNote.Services.CoursService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CoursServiceImplementation implements CoursService {

    @Autowired
    private CoursRepository coursRepository;

    public Cours createCours(Cours cours) {
        return coursRepository.save(cours);
    }

    public List<Cours> getAllCours() {
        return coursRepository.findAll();
    }

    public Optional<Cours> getCoursById(Long coursId) {
        return coursRepository.findById(coursId);
    }
}
