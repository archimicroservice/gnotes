package com.tpSI.gNote.ServiceImplementations;

import com.tpSI.gNote.Entities.Semestre;
import com.tpSI.gNote.Repositories.SemestreRepository;
import com.tpSI.gNote.Services.SemestreService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class SemestreServiceImplementation implements SemestreService {

    @Autowired
    private SemestreRepository semestreRepository;

    public List<Semestre> getAllSemestres() {
        return semestreRepository.findAll();
    }

    public Optional<Semestre> getSemestreById(Long semestreId) {
        return semestreRepository.findById(semestreId);
    }
}
