package com.tpSI.gNote.ServiceImplementations;


import com.tpSI.gNote.Entities.Inscription;
import com.tpSI.gNote.Repositories.InscriptionRepository;
import com.tpSI.gNote.Services.InscriptionService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class InscriptionServiceImplementation implements InscriptionService {

    @Autowired
    private InscriptionRepository inscriptionRepository;

    public Inscription saveInscription(Inscription inscription) {
        return inscriptionRepository.save(inscription);
    }

    public List<Inscription> getInscriptionsByCourse(Long courseId) {
        return inscriptionRepository.findByCoursId(courseId);
    }

    public List<Inscription> getInscriptionsByStudent(Long studentId) {
        return inscriptionRepository.findByEtudiantId(studentId);
    }
}
