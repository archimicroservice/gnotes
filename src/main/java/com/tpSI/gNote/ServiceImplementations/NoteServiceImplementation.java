

package com.tpSI.gNote.ServiceImplementations;
import com.tpSI.gNote.Entities.Note;
import com.tpSI.gNote.Repositories.NoteRepository;
import com.tpSI.gNote.Services.NoteService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class NoteServiceImplementation implements NoteService {

    @Autowired
    private NoteRepository noteRepository;

    public Note saveNote(Note note) {
        return noteRepository.save(note);
    }

    public List<Note> getNotesByStudentAndCourse(Long studentId, Long courseId) {
        return noteRepository.findByEtudiantIdAndCoursId(studentId, courseId);
    }

    public Double calculateAverageNote(Long studentId) {
        List<Note> studentNotes = noteRepository.findByEtudiantId(studentId);

        if (studentNotes.isEmpty()) {
            return 0.0;
        }

        double sum = 0;
        for (Note note : studentNotes) {
            sum += note.getNoteObtenue();
        }

        return sum / studentNotes.size();
    }
}
