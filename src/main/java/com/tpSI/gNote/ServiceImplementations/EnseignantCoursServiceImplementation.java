package com.tpSI.gNote.ServiceImplementations;

import com.tpSI.gNote.Entities.EnseignantCours;
import com.tpSI.gNote.Repositories.EnseignantCoursRepository;
import com.tpSI.gNote.Services.EnseignantCoursService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class EnseignantCoursServiceImplementation implements EnseignantCoursService {

    @Autowired
    private EnseignantCoursRepository enseignantCoursRepository;

    public EnseignantCours saveEnseignantCours(EnseignantCours enseignantCours) {
        return enseignantCoursRepository.save(enseignantCours);
    }

    public List<EnseignantCours> getEnseignantCoursByCourse(Long courseId) {
        return enseignantCoursRepository.findByCoursId(courseId);
    }

    public List<EnseignantCours> getEnseignantCoursByEnseignant(Long enseignantId) {
        return enseignantCoursRepository.findByEnseignantId(enseignantId);
    }

    public Optional<EnseignantCours> getEnseignantCoursById(Long enseignantCoursId) {
        return enseignantCoursRepository.findById(enseignantCoursId);
    }
}
