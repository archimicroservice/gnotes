package com.tpSI.gNote.ServiceImplementations;

import com.tpSI.gNote.Entities.Enseignant;
import com.tpSI.gNote.Repositories.EnseignantRepository;
import com.tpSI.gNote.Services.EnseignantService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class EnseignantServiceImplementation implements EnseignantService {

    @Autowired
    private EnseignantRepository enseignantRepository;

    public Enseignant createEnseignant(Enseignant enseignant) {
        return enseignantRepository.save(enseignant);
    }

    public List<Enseignant> getAllEnseignants() {
        return enseignantRepository.findAll();
    }

    public Optional<Enseignant> getEnseignantById(Long enseignantId) {
        return enseignantRepository.findById(enseignantId);
    }
}
