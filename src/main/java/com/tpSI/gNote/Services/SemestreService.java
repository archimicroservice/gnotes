package com.tpSI.gNote.Services;


import com.tpSI.gNote.Entities.Semestre;

import java.util.List;
import java.util.Optional;

public interface SemestreService {
    public List<Semestre> getAllSemestres();
    public Optional<Semestre> getSemestreById(Long semestreId);
}
