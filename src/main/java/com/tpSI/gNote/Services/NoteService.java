package com.tpSI.gNote.Services;


import com.tpSI.gNote.Entities.Note;

import java.util.List;

public interface NoteService {
    public Note saveNote(Note note);
    public Double calculateAverageNote(Long studentId);
    public List<Note> getNotesByStudentAndCourse(Long studentId, Long courseId);
}
