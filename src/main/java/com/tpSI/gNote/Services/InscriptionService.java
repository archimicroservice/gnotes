package com.tpSI.gNote.Services;


import com.tpSI.gNote.Entities.Inscription;

import java.util.List;

public interface InscriptionService {
    public Inscription saveInscription(Inscription inscription);
    public List<Inscription> getInscriptionsByCourse(Long courseId);
    public List<Inscription> getInscriptionsByStudent(Long studentId);
}
