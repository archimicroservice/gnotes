package com.tpSI.gNote.Services;


import com.tpSI.gNote.Entities.EnseignantCours;

import java.util.List;
import java.util.Optional;

public interface EnseignantCoursService {
    public EnseignantCours saveEnseignantCours(EnseignantCours enseignantCours);
    public List<EnseignantCours> getEnseignantCoursByCourse(Long courseId);
    public List<EnseignantCours> getEnseignantCoursByEnseignant(Long enseignantId);
    public Optional<EnseignantCours> getEnseignantCoursById(Long enseignantCoursId);
}
