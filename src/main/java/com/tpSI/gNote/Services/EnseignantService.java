package com.tpSI.gNote.Services;


import com.tpSI.gNote.Entities.Enseignant;

import java.util.List;
import java.util.Optional;

public interface EnseignantService {
    public Enseignant createEnseignant(Enseignant enseignant);
    public List<Enseignant> getAllEnseignants();
    public Optional<Enseignant> getEnseignantById(Long enseignantId);
}
