package com.tpSI.gNote.Services;



import com.tpSI.gNote.Entities.Cours;

import java.util.List;
import java.util.Optional;

public interface CoursService {
    public Cours createCours(Cours cours);
    public List<Cours> getAllCours();
    public Optional<Cours> getCoursById(Long coursId);
}
